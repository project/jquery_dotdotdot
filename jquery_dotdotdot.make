core = 7.x
api = 2

; jQuery.dotdotdot plugin (jquery.dotdotdot) v1.8.3
libraries[jquery.dotdotdot][type] = libraries
libraries[jquery.dotdotdot][download][type] = git
libraries[jquery.dotdotdot][download][url] = git@github.com:FrDH/jQuery.dotdotdot
libraries[jquery.dotdotdot][download][tag] = v1.8.3
libraries[jquery.dotdotdot][directory_name] = jquery.dotdotdot
libraries[jquery.dotdotdot][destination] = libraries
